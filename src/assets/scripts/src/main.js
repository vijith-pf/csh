var viewportHeight = $(window).height();
var viewportWidth = $(window).width();
var playersArr = [];
var _player;

var commonInits = {
  initLoad: function() {
    $('body').addClass('loaded');
  },
  initSelect2: function() {
    // Custom Select
    $('.selectpicker').select2({
        placeholder: false
    });
  },
  searchToggle: function () {
    $('#search-trigger').click(function () {
      //window.matchMedia('(min-width: 991px)').matches
        $('.search-wrap-input').addClass('active');
        $('.mobile-search-wrap').addClass('active');
      if (viewportWidth > 991) {
        // $('.search-wrap-input').addClass('active');
      } else {
        // $('.mobile-search-wrap').addClass('active');
      }
    });
  },
  accesibility: function() {
      $('#aPlusPlus').on('click', function () {
          $('body').addClass('aplusplus');
          $('body').removeClass('aplus');
          $('body').removeClass('aminusminus');
          $('body').removeClass('aminus');
      });
      $('#aPlus').on('click', function () {
          $('body').addClass('aplus');
          $('body').removeClass('aplusplus');
          $('body').removeClass('aminus');
          $('body').removeClass('aminusminus');
      });
      $('#aMinus').on('click', function () {
          $('body').addClass('aminus');
          $('body').removeClass('aplusplus');
          $('body').removeClass('aplus');
          $('body').removeClass('aminusminus');
      });
      $('#aMinusMinus').on('click', function () {
          $('body').addClass('aminusminus');
          $('body').removeClass('aminus');
          $('body').removeClass('aplusplus');
          $('body').removeClass('aplus');
      });
      $('#aA').on('click', function () {
          $('body').removeClass('aminusminus');
          $('body').removeClass('aminus');
          $('body').removeClass('aplusplus');
          $('body').removeClass('aplus');
      });
      $('#contrast_1').on('click', function () {
        $('body').removeClass('color-blind');
        $('body').removeClass('green-weakness');
      });
      $('#contrast_2').on('click', function () {
        $('body').addClass('color-blind');
        $('body').removeClass('green-weakness');
      });
      $('#contrast_3').on('click', function () {
        $('body').addClass('green-weakness');
        $('body').removeClass('color-blind');
      });
  },
  collapseExpand: function() {
    //Collapsing panel
    $('.collapsing-panel .panel-head h6').prepend('<i class="icomoon icon-toggle"></i> ');
    $('.collapsing-panel .panel-head').click(function(e) {

      $('.panel-details').slideUp();
      $(this).next('.panel-details').slideDown();

      e.preventDefault();
    });

    $('.accordion .card-header').click(function(){
      $('.accordion .card').removeClass('show');
      setTimeout(function(){
          if( $(this).hasClass('collapsed') != true ){
            $(this).parent().addClass('show');
          }
      }, 10);
    });

    $(document).ready(function(){
      setTimeout(function(){
        $('.accordion .card-header').each(function(){
          if($(this).hasClass('collapsed') != true){
            $(this).parent().addClass('show');
          }
        });
      }, 100);
    });

  },
  counterUp: function() {
    $('.counter').counterUp({
      delay: 10,
      time: 1000
    });
  },
  sectionBg: function() {
    var sectionBg = $('[data-bg]');
    $(sectionBg).each(function(){
      var bg = $(this).attr('data-bg');
      $(this).css("background-image", "url(" + bg + ")").addClass('add-overlay');
    });
  },
  scrollToElements: function() {
    $(".scroll-to-elem").click(function() {
      var currentElem = $(this).attr('href')
      $([document.documentElement, document.body]).animate({
          scrollTop: $(currentElem).offset().top - 107
      }, 800);
    });
  },
  disablePageScroll: function () {
    $.scrollLock( true );
  },
  enablePageScroll: function () {
    $.scrollLock( false );
  },
  toggleShare: function() {
    $('.share-icons-trigger').click(function(){
      $(this).toggleClass('active');
    });
  },
  toggleInsurance: function() {
    $('.insurance > .cta-row a').click(function(e){
      e.preventDefault();
      $(this).parents('.insurance').parent().siblings().find('.insurance').removeClass('active');
      $(this).parents('.insurance').toggleClass('active');
    });
    $('.insurance .popup-over .cta-row a').click(function(e){
      e.preventDefault();
      $('.insurance').removeClass('active');
    });
  },
  outerClick: function() {
    $(document).mouseup(function(e){
        $('.insurance').removeClass('active');
    });
    $(document).on("click", function (event) {
      if (!$(event.target).closest(".search-wrap-input").length && !$(event.target).closest(".search-trigger").length) {
        $(".search-wrap-input").removeClass('active');
      }
    });

    $(document).on("click", function (event) {
      if (!$(event.target).closest(".mobile-search-wrap").length && !$(event.target).closest(".search-trigger").length) {
        $(".mobile-search-wrap").removeClass('active');
      }
    });

  },
  datePicker: function() {
    $('[data-toggle="datepicker"]').datepicker({
      autoHide: true
    });
  },
  sidebarMobile: function() {

    //var deliveryPackages = $('.sidebar-cols .sidebar-inner .info-box');
    var deliveryPackages = $('.sidebar-cols .sidebar-inner .info-box-slider');
    var sidebarMenu = $('.sidebar-cols .sidebar-inner .sidebar-menu');
    var toggleMenu = $('.sidebar-cols .sidebar-inner .bottom-space-sm');
    deliveryPackages.clone().appendTo('.sidebar-cols .content-col');

    $('.sidebar-link li').each(function(){
      if( $(this).hasClass('active') ){
        $(this).removeClass('hidden');
      } else {
        $(this).addClass('hidden');
      }
    });

    $('.sidebar-link li.active a').click(function(e){
      $(this).parent().toggleClass('show-mobile-navs');
      $(this).parent().siblings().toggleClass('hidden');
      e.preventDefault();
    });

  },
  stickyPanel: function() {
    var window_top = $(window).scrollTop();
    var top = $('#stick-here').offset().top;
    if (window_top > top) {
        $('#stickThis').addClass('stick');
        // $('#stick-here').height($('#stickThis').outerHeight());
    } else {
        $('#stickThis').removeClass('stick');
        // $('#stick-here').height(0);
    }
  }
}

var menuLine = {
    showMenuLine: function () {
        $('.top-nav.bg-primary').addClass("active");
    },
    hideMenuLine: function () {
        $('.top-nav.bg-primary').removeClass("active");
    }

}

var initSlickSliders = {
    initHero: function () {
        $(".hero-slider").slick({
          autoplay: true,
          autoplaySpeed: 3000,
            prevArrow: '<button class="slide-arrow prev-arrow"><i class="iconmoon icon-arrow-left" aria-hidden="true"></i></button>',
            nextArrow: '<button class="slide-arrow next-arrow"><i class="iconmoon icon-arrow-right" aria-hidden="true"></i></button>',
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        arrows: false
                    }
                }
            ]
        });
    },
    initCardSpecialistsHomepage: function () {
        $("#cardSpecialistsHomepage").slick({
            slidesToShow: 3,
            slidesToScroll: 3,
            // autoplay: true,
            prevArrow: '<button class="slide-arrow prev-arrow"><i class="iconmoon icon-arrow-left" aria-hidden="true"></i></button>',
            nextArrow: '<button class="slide-arrow next-arrow"><i class="iconmoon icon-arrow-right" aria-hidden="true"></i></button>',
              responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        arrows: false
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false
                    }
                }
            ]
        });
    },
    initCardSpecialists: function () {
        // $("#cardSpecialists").slick({
        //     slidesToShow: 2,
        //     slidesToScroll: 2,
        //     prevArrow: '<button class="slide-arrow prev-arrow"><i class="iconmoon icon-arrow-left" aria-hidden="true"></i></button>',
        //     nextArrow: '<button class="slide-arrow next-arrow"><i class="iconmoon icon-arrow-right" aria-hidden="true"></i></button>',
        //     responsive: [
        //         {
        //             breakpoint: 768,
        //             settings: {
        //                 slidesToShow: 1,
        //                 slidesToScroll: 1,
        //                 prevArrow: false,
        //                 nextArrow: false,
        //                 arrows: false
        //             }
        //         }
        //     ]
        // });
        $("#cardSpecialists").slick({
            slidesToShow: 2,
            slidesToScroll: 2,
            // autoplay: true,
            prevArrow: '<button class="slide-arrow prev-arrow"><i class="iconmoon icon-arrow-left" aria-hidden="true"></i></button>',
            nextArrow: '<button class="slide-arrow next-arrow"><i class="iconmoon icon-arrow-right" aria-hidden="true"></i></button>',
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        arrows: false
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false
                    }
                }
            ]
        });
    },
    initCardBookingBestSellers: function () {
        $("#bestSellers-card-slider").slick({
            slidesToShow: 4,
            slidesToScroll: 4,
            prevArrow: '<button class="slide-arrow prev-arrow"><i class="iconmoon icon-arrow-left" aria-hidden="true"></i></button>',
            nextArrow: '<button class="slide-arrow next-arrow"><i class="iconmoon icon-arrow-right" aria-hidden="true"></i></button>',
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        arrows: false
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        arrows: false
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false
                    }
                }
            ]
        });
    },
    initCardBookingAllPackages: function () {
        $("#allPackages-card-slider").slick({
            slidesToShow: 4,
            slidesToScroll: 4,
            prevArrow: '<button class="slide-arrow prev-arrow"><i class="iconmoon icon-arrow-left" aria-hidden="true"></i></button>',
            nextArrow: '<button class="slide-arrow next-arrow"><i class="iconmoon icon-arrow-right" aria-hidden="true"></i></button>',
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        arrows: false
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        arrows: false
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false
                    }
                }
            ]
        });
    },
    initThumbSlider: function () {
        $(".thumbnail-slider").slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            prevArrow: '<button class="slide-arrow prev-arrow"><i class="iconmoon icon-arrow-left" aria-hidden="true"></i></button>',
            nextArrow: '<button class="slide-arrow next-arrow"><i class="iconmoon icon-arrow-right" aria-hidden="true"></i></button>',
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                },
                {
                    breakpoint: 736,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        // prevArrow: false,
                        // nextArrow: false
                    }
                }
            ]
        });
    },
    videoPlayer: function (container, videoId) {
      console.log(container, videoId);

      
      
    },
    generalVideoSlider: function () {
      

      var player, playing = false;
      var slideWrapper = $(".video-slider");
      var playersArr = [];
      function onYouTubeIframeAPIReady() {

          var tag = document.createElement('script');

          tag.src = "https://www.youtube.com/iframe_api";
          var firstScriptTag = document.getElementsByTagName('script')[0];
          firstScriptTag.parentNode.insertBefore(tag, firstScriptTag); 

          var slideItems = $(slideWrapper).find('.video-wrapper');
          slideItems.each(function(){
            var videoId = $(this).data('video-id');
            var playerId = $(this).data('player-id');
            _player = new YT.Player(playerId, {
              height: '360',
              width: '640',
              videoId: videoId,
              playerVars: {
                autoplay: 0,
                controls: 1,
                autohide: 0,
                enablejsapi: 1,
                modestbranding: 1,
                rel: 0,
                showInfo: 0,
                vq: 'hd1080'
              },
              events: {
                  'onStateChange': onPlayerStateChange
              }
            });
        
            playersArr.push(_player);
        
          });
      }

      function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING) {
          $('#testimonials .caption').addClass('hide');
          playing = true;
        } else if(event.data == YT.PlayerState.PAUSED){
          $('#testimonials .caption').removeClass('hide');
          var frameElem = window.frames['myIFrame'].contentWindow.document.getElementById('myIFrameElemId');
          var elem = document.getElementsByClassName("ytp-pause-overlay");
          playing = false;
        }
      }

      function pauseVideo() {
        $.each( playersArr, function( i, val ) {
          playersArr[i].pauseVideo();
        });
      }

      slideWrapper.on("init", function (event, slick) {
        onYouTubeIframeAPIReady();
      });

      slideWrapper.on("afterChange", function (event, slick) {
        slick = $(slick.$slider);
        pauseVideo();
      })
  
      slideWrapper.slick({
        lazyLoad: 'ondemand',
        autoplaySpeed: 4000,
        speed: 600,
        infinite: false,
        dots: false,
        cssEase: "cubic-bezier(0.87, 0.03, 0.41, 0.9)",
        prevArrow: '<button class="slide-arrow prev-arrow"><i class="iconmoon icon-arrow-left" aria-hidden="true"></i></button>',
        nextArrow: '<button class="slide-arrow next-arrow"><i class="iconmoon icon-arrow-right" aria-hidden="true"></i></button>',
      });


    },
    initInfobox: function () {
        $(".info-box-slider").slick({
            prevArrow: '<button class="slide-arrow prev-arrow"><i class="iconmoon icon-arrow-left" aria-hidden="true"></i></button>',
            nextArrow: '<button class="slide-arrow next-arrow"><i class="iconmoon icon-arrow-right" aria-hidden="true"></i></button>',
            autoplay: true,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        arrows: false
                    }
                }
            ]
        });
    },
    initVideoSlider: function () {
      $('#video-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2000,
        dots: false,
        infinite: true,
        adaptiveHeight: true,
        // arrows: false,
        prevArrow: '<button class="slide-arrow prev-arrow"><i class="iconmoon icon-arrow-left" aria-hidden="true"></i></button>',
        nextArrow: '<button class="slide-arrow next-arrow"><i class="iconmoon icon-arrow-right" aria-hidden="true"></i></button>',
      });

      if ($('#main-slider .slick-active').length) {
        var video = $('#main-slider .slick-active').find('iframe').get(0).play();
        $('#main-slider').on('afterChange', function (event, slick, currentSlide, nextSlide) {
          $('#main-slider .slick-slide').find('video').get(0).pause();
          var video = $('#main-slider .slick-active').find('video').get(0).play();
        });
      }
    }

}

var initMenu = {
  navDropDown: function() {
    if(viewportWidth > 991){

      $(".nav-items > li.nav-dropdown").mouseenter(function() {
        menuLine.showMenuLine();
        $(this).addClass('active');
        var position = $('.top-nav').position();
        // $('.wide-dropdown .nav-dropdown-row').css({'top': position.top + 63 });
      }).mouseleave(function() {
        menuLine.hideMenuLine();
        $(this).removeClass('active');
      });

      $(".nav-dropdown-full-panel").mouseenter(function() {
        menuLine.showMenuLine();
        $(this).addClass("active");
      }).mouseleave(function() {
        menuLine.hideMenuLine();
        $(this).removeClass("active");
      });

      $(".nav-dropdown-row, .nav-items > li > a").mouseenter(function() {
        // commonInits.disablePageScroll();
      }).mouseleave(function() {
        // commonInits.enablePageScroll();
      });

    } else {
      $(".nav-items > li.nav-dropdown").click(function(){
        $(this).siblings().removeClass('active');
        $(this).toggleClass('active');
      });
    }
    var dropdownMenu = $('.dropdown-img');
    $(dropdownMenu).each(function() {
        $(this).parent().find('.nav-dropdown-row').prepend($(this));
    });
    $('.nav-dropdown-row a').click(function(){
      $('.nav-dropdown').removeClass('active');
    });
    $(".simple-dropdown.nav-dropdown, .wide-dropdown.nav-dropdown").removeClass("active");
  },
  
  mobileMenu: function() {
    // $(".search-trigger").clone().prependTo(".mobile-nav");
    $(".humburger-trigger, .close-menu-trigger").click(function () {
      $(".navbar-default").toggleClass("active");
    });
  },
  mobileSearchTrigger: function () {
    $("#search-trigger-mobile").click(function () {
      $(".mobile-search-wrap").toggleClass("active")
    });
  }
  

}

var initValidations = {
  initMethods: function() {
    $.validator.addMethod("namefield", function(value, element) { return this.optional(element) || /^[a-z\'\s]+$/i.test(value); }, "Invalid Characters");
    $.validator.addMethod("phonefield", function(value, element) { return this.optional(element) || /^[0-9\+\s]+$/i.test(value); }, "Invalid phone number");
    $.validator.addMethod("msgfield", function(value, element) { return this.optional(element) || /^[a-z\0-9\,.'"!()+@\s]+$/i.test(value); }, "Invalid Characters");
    initValidations.initApplicationValiation();
  },
  initApplicationValiation: function() {
    $("#application").validate({
      rules: {
        name: {
          required: true,
          minlength: 2,
          maxlength: 25,
          namefield: true
        },
        email: {
          required: true,
          maxlength: 35,
          email: true
        },
        phone: {
          required: true,
          phonefield: true,
          minlength: 10,
          maxlength: 25,
        },
        productOptions: {
          required: function() {
            return $('[name="productOptions"]:checked').length === 0;
          }
        }
      },
      messages: {
        userGender: {
          required: "please select an option"
        },
      },
      submitHandler: function(form) {
        $('.apply-form .detail-box .btn').addClass('disabled');
        $(form).submit();
      }
    });
  },
  intFileUpload: function(){
    $("#custom-file-input").on('change', function () {
      $("#selectedFile").empty();
      var fp = $("#custom-file-input");
      var lg = fp[0]
        .files.length; // get length
      var items = fp[0].files;
      var fragment = "";
      if (lg > 0) {
        for (var i = 0; i < lg; i++) {
          var fileName = items[i].name; // get file name
          var fileSize = items[i].size; // get file size
          var fileType = items[i].type; // get file type
          // append li to UL tag to display File info
          fragment += "<li>" + fileName + " " + fileSize + " bytes. Type :" + fileType + "</li>";
        }
        $("#selectedFile").append(fragment);
      }
    });
  }
}


$(document).ready(function() {
  initMenu.navDropDown();
  initMenu.mobileMenu();
  initMenu.mobileSearchTrigger();
  commonInits.initSelect2();
  commonInits.collapseExpand();
  commonInits.counterUp();
  commonInits.sectionBg();
  commonInits.scrollToElements();
  commonInits.toggleShare();
  commonInits.toggleInsurance();
  commonInits.outerClick();
  commonInits.datePicker();
  commonInits.sidebarMobile();
  commonInits.accesibility();
  commonInits.searchToggle();

  initValidations.initMethods();
  initValidations.intFileUpload();
  initSlickSliders.initHero();
  initSlickSliders.initCardSpecialists();
  initSlickSliders.initCardSpecialistsHomepage();
  initSlickSliders.initThumbSlider();
  initSlickSliders.initCardBookingBestSellers();
  initSlickSliders.initCardBookingAllPackages();
  initSlickSliders.initInfobox();
  initSlickSliders.initVideoSlider();
  if ($('#stickThis').length) {
    commonInits.stickyPanel();
  }
});

if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {
  window.onYouTubeIframeAPIReady = function() {
    initSlickSliders.generalVideoSlider();
  };

  $.getScript('//www.youtube.com/iframe_api');
} else {
  initSlickSliders.generalVideoSlider();
}

$(window).scroll(function(e){
  if ($('#stickThis').length) {
    commonInits.stickyPanel();
  }
});

$(window).load(function(){
  commonInits.initLoad();
});
                          
/*
    Make cards clickable
    ====================
*/

$('.video-deck .card-body').on('click', function () {
  $(this).parent().find("a").trigger('click');
});

/*

    Set caption from card text
    ==========================
*/

$('.video-deck a').fancybox({
  caption: function (instance, item) {
    return $(this).parent().find('.video-title').html();
  }
});

function printFunction() {
  window.print();
}

$(window).scroll(function () {
  var scroll = $(window).scrollTop();

  //>=, not <=
  if (scroll >= 600) {
    //clearHeader, not clearheader - caps H
    $("#stickThis").addClass("stickremove");
  }else{
    $("#stickThis").removeClass("stickremove");
  }
}); //missing );

